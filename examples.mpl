# Change to the path of the file rationalSOS.mpl
read("rationalSOS.mpl");
with(rationalSOS);

# Display tables of any size
interface(rtablesize=infinity);

#######################################################################
# Example 1 - See Example 2.1 in [1]
#######################################################################

# We define a polynomial f as the sum of two squares.
p1 := x^2 + 3*x*y - 5*x*z+ 2z^2;
p2 := 3x^2 - 2*x*z +y*z+5*y^2;
f := expand(p1^2 + p2^2);

out := exactSOS(f);
p := out[1][1]*out[2][1]^2+out[1][2]*out[2][2]^2;
expand(p);

#######################################################################
# Example 2 - See Example 3.3 in [1]
#######################################################################

f := 3618*x^8+468*x^7*y+6504*x^7*z-1104*x^6*y^2+2616*x^6*y*z+57481*x^6*z^2-144*x^5*y^3-1652*x^5*y^2*z-16440*x^5*y*z^2+23420*x^5*z^3+160*x^4*y^4+1392*x^4*y^3*z-2520*x^4*y^2*z^2-28448*x^4*y*z^3+91320*x^4*z^4-240*x^3*y^4*z+1728*x^3*y^3*z^2+10524*x^3*y^2*z^3-85500*x^3*y*z^4+34740*x^3*z^5-3696*x^2*y^3*z^3+28920*x^2*y^2*z^4-15192*x^2*y*z^5-57267*x^2*z^6+720*x*y^4*z^3-3312*x*y^3*z^4-3168*x*y^2*z^5+26352*x*y*z^6-40176*x*z^7+720*y^4*z^4+864*y^3*z^5-9072*y^2*z^6+46656*z^8;
# We force the coefficients of non-rational elements to be 0. This makes the problem easier to solve, but it can miss solutions.
out := exactSOS(f, forceRational = "yes", digits = 2);
p := 0;
for i from 1 to 6 do p := p + out[1][i]*out[2][i]^2 end:
expand(p);

#######################################################################
# Example 3 - See Theorem 5.1
#######################################################################

p1 := 2*RootOf(_Z^3-2)^2*z*x^2+8*RootOf(_Z^3-2)^2*z^2*x+2*x^3*RootOf(_Z^3-2)+8*x^2*RootOf(_Z^3-2)*z-2*w^2*x-14*w^2*z+126*w*x*z+28*x^3-42*x^2*z+3*x*y^2-42*x*y*z-296*x*z^2-294*y*z^2-700*z^3;
p2 := 2*RootOf(_Z^3-2)^2*x^3+22*RootOf(_Z^3-2)^2*z*x^2+56*RootOf(_Z^3-2)^2*z^2*x+6*x^3*RootOf(_Z^3-2)+24*x^2*RootOf(_Z^3-2)*z+2*w^2*z-42*w*x*z-10*x^3+2*x^2*z-x*y^2+28*x*z^2+42*y*z^2+100*z^3;
p3 := 2*w*RootOf(_Z^3-2)^2*x^2+8*x*w*RootOf(_Z^3-2)^2*z+2*y*RootOf(_Z^3-2)*x^2+8*x*y*RootOf(_Z^3-2)*z+42*x^2*z+168*x*z^2;
f := p1^2 + p2^2 + p3^2;
sSym := [solve({p1 = 0, p2 = 0, p3 = 0})];

# We provide explicit solutions to the equation f=0 to avoid randomness.
s31 := evalSolution(sSym[3], {w = 1, y = 1});
s32 := evalSolution(sSym[3], {w = 0, y = 1});
s33 := evalSolution(sSym[3], {w = 1, y = 0});
s34 := evalSolution(sSym[3], {w = 1, y = -1});
s11 := evalSolution(sSym[1], {x = 1, w = 1});
s12 := evalSolution(sSym[1], {x = 1, w = 2});
s13 := evalSolution(sSym[1], {x = 1, w = 3});

# This points can easily be verified to be real points and solutions of f=0, for example
evalf(s11);
simplify(eval(f, s11));

# We call rationalSOS using the explicit solutions.
# (if you use random solutions instead, the computations may not finish or the dimension reduction may not be enough to solve the problem).
out := exactSOS(f, zeros = [s31, s32, s33, s34, s11, s12, s13, sSym[2], sSym[4]], traceEquations = "no", incremental = "yes", printLevel = 2);

# The problem was completely solved without using the numerical solver. 
# There is only one possible matrix, and it has non-ratioanl entries, hence there is no
# rational SOS decomposition for f.
p := 0;
for i from 1 to 6 do p := p + out[1][i]*out[2][i]^2 end;
simplify(f-p);

#######################################################################
# Example 4 - Motzkin polynomial
#######################################################################

f:= x^4*y^2+x^2*y^4-3*x^2*y^2*z^2+2*z^6;
exactSOS(f, traceEquations = "no");

#######################################################################
# Example 5 - Scheiderer counterexample
#######################################################################

f := x^4+x*y^3+y^4-3*x^2*y*z-4*x*y^2*z+2*x^2*z^2+x*z^3+y*z^3+z^4;

# If we use trace equations, we get that there is no solution.
# f does not admit a rational SOS decomposition.
out1 := exactSOS(f);

# If we dont use trace equations, the solution found is semi-definite
# positive and cannot be rounded to a rational solution.
out2 := exactSOS(f, traceEquations = "no");
eig(out2[3]);

